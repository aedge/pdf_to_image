const path = require('path');
const pdf = require('pdf-poppler');

const readline = require('readline')

let read = readline.createInterface({
    input:process.stdin,
    output:process.stdout
})
read.question("请将pdf放入根目录下的pdf文件夹，然后输入pdf名称，开始转换，无需后缀名\n",fileName=>{
    read.close()
    let file = path.resolve(`./pdf/${fileName}.pdf`)
    let opts = {
        format: 'png',
        out_dir: path.resolve("./pdf"),
        out_prefix: fileName,
        page: null
    }
    pdf.convert(file, opts)
    .then(res => {
        console.log('转换成功');
        process.exit(0);
    })
    .catch(error => {
        console.error(error);
        process.exit(1);        
    })
})
